@extends('layouts.master')
@section('title', 'List')
@section('css')
@parent
    <link rel="stylesheet" href="css/main.css">
@endsection
@section('content')
    @if (Session::has('message'))
        <div class="alert alert-success">
            {{ Session::get('message') }}
        </div>
    @endif
    <a href="{{ url('people/create') }}">
        <button type="button" class="btn btn-success">Create</button>
    </a>
<table class="table table-dark">
    <thead>
        <th>ID</th>
        <th>Firstname</th>
        <th>Lastname</th>
        <th>Age</th>
        <th>Created_at</th>
        <th>Updated_at</th>
        <th>Actions</th>

    </thead>
    <tbody>
        @foreach ($people as $p)
        <tr>
            <td>{{ $p->id }}</td>
            <td>{{ $p->fname }}</td>
            <td>{{ $p->lname }}</td>
            <td>{{ $p->age }}</td>
            <td>{{ date('Y-m-d H:i:s', strtotime($p->created_at)) }}</td>
            <td>{{ date('Y-m-d H:i:s', strtotime($p->updated_at)) }} </td>
            <td>
                <div class="form-inline">
                    <a href="{{ url('people/' . $p->id . '/edit') }}">
                        <button type="button" class="btn btn-primary">Edit</button>
                    </a>
                    <form action="{{ url('people/' . $p->id) }}" method="post">
                        @csrf
                        @method('delete')
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </div>

            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection